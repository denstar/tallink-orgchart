var path = require('path');

module.exports = {
  entry: {
    'bundle': ['./app/main.js']
  },
  output: {
    path: './app/',
    filename: '[name].js'
  },
  devtool: 'source-map',
  module: {
    loaders: [
      {
        loader: 'babel',
        query: {
          presets: ['es2015']
        }
      }
    ]
  }
};
