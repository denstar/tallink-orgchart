export function getAllEmployees()
{
	return ajaxLogging($.ajax({
		url:settings.urls.employee
	}));
}

export function getOrgChart()
{
	return ajaxLogging($.ajax({
		url:settings.urls.ceo
	}));
}

export function addPosition(position)
{
	return ajaxLogging($.ajax({
		headers: { 
			'Accept': 'application/json',
			'Content-Type': 'application/json' 
		},
		type: 'POST',
		url:settings.urls.position + (position.managerId || 'ceo') +'/',
		data: JSON.stringify(position)
	}));
}

export function deletePosition(positionId)
{
	return ajaxLogging($.ajax({
		type: 'DELETE',
		url:settings.urls.position + positionId +'/'
	}));
}

export function publishEmployee(employee)
{
	return ajaxLogging($.ajax({
		headers: { 
			'Accept': 'application/json',
			'Content-Type': 'application/json' 
		},
		type: 'POST',
		url:settings.urls.employee,
		data: JSON.stringify(employee)
	}));
}

function ajaxLogging(ajax)
{
	return ajax
		.done(function(data)
		{
			console.log(data);
		})
		.fail((xhr, textStatus, errorThrown) =>
		{
			console.error([xhr, textStatus, errorThrown]);
		});
}
