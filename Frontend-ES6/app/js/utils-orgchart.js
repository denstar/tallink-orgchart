export function makeChart(obj)
{
	var CEO = obj ? makePosition(obj) : null,
		chart = CEO;

	return chart;
}

export function makePosition(position)
{
	position.name = position.employee.title;
	position.children = position.subordinates || [];

	if (!position.children.length)
		delete position.children;

	for (var k in position.children)
		position.children[k] = makePosition(position.children[k]);

	return position;
}
