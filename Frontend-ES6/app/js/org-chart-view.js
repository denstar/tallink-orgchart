import OrgChart from './vendor/orgchart.min.js';
import { makeChart } from './utils-orgchart.js';
import { initEditChartView } from './edit-chart-view.js';

export function loadChartView(data)
{
	chartData = makeChart(data);

	if (chartData)
		return new OrgChart({
			'chartContainer': '#chart-container',
			'data' : chartData,
			'nodeContent': 'title',
			'nodeTemplate': nodeTemplate,
			'createNode': createNode
		});
}

function createNode(nodeDiv, data) {
	$(nodeDiv).data('item', data);
}

export function initOrgChartView(data)
{
	var oc = loadChartView(data);

	initEditChartView(oc);

	return oc;
}

function showSigningAuthorityHtml(val)
{
	return val ? '<br><b>Signing authority</b>' : '';
}

function nodeTemplate(data)
{
	var strSigningAuthority = showSigningAuthorityHtml(data['signingAuthority']),
		strStyle = data['signingAuthority'] ? ' class="content" style="height:30pt;"' : ' class="content"';

	return '<div class="title">' + data['name'] + '</div>'
		+ '<div'+ strStyle +'>' + (data['title'] || '') + strSigningAuthority + '</div>'
}
