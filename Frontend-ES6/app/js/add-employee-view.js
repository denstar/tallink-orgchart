import * as rest from './orgchart-rest-api.js';
import { populateNewNodeSelect } from './edit-chart-view.js';

export function initAddEmployeeView()
{
	$('#btn-add-employee').on('click', function() {
		var employee = { title: $('#new-employee').val() };
		if (!employee.title)
		{
			alert('Please input name for the new employee');
			return;
		}

		publishEmployee(employee);
	});
}

function publishEmployee(employee)
{
	console.log(employee);

	rest.publishEmployee(employee)
		.done(function(data)
		{
			employees.push(data);
			$('#new-employee').val('');
			$('#add-employee-status').text('Employee added');
			$('#add-employee-status').fadeIn().delay(500).fadeOut();

			populateNewNodeSelect(data);
		})
		.fail( function(xhr, textStatus, errorThrown) {
			$('#add-employee-error').text(getErrorMessage(xhr));
			$('#add-employee-error').fadeIn().delay(500).fadeOut();
		});
}

function getErrorMessage(xhr)
{
	try
	{
		return JSON.parse(xhr.responseText).message.substring(0, 30);
	}
	catch (e)
	{
		return ((xhr || {}).responseText || '').substring(0, 30);
	}
}