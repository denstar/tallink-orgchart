import * as rest from './orgchart-rest-api.js';
import { loadChartView } from './org-chart-view.js';
import { makePosition } from './utils-orgchart.js';
import { clearFilterResult } from './filter-node-view.js';

export function initEditChartView(oc)
{
	if (oc)
	{
		var chartContainer = $(oc.chartContainer);

		chartContainer.on('click', '.node', function() {
			var $this = $(this);
			$('#selected-node').text($this.find('.title').text()).data('node', $this);
		});

		chartContainer.on('click', '.orgchart', function(event) {
			if (!$(event.target).closest('.node').length) {
				$('#selected-node').text('');
			}
		});
	}

	$('input[name="chart-state"]').on('click', function() {
		$('.orgchart').toggleClass('edit-state', this.value !== 'view');
		$('#edit-panel').toggleClass('edit-state', this.value === 'view');
		if ($(this).val() === 'edit') {
			$('.orgchart').find('tr').removeClass('hidden')
				.find('td').removeClass('hidden')
				.find('.node').removeClass('slide-up slide-down slide-right slide-left');
		} else {
			$('#btn-reset').trigger('click');
		}
	});

	$('input[name="node-type"]').on('click', function() {
		var $this = $(this);
		if ($this.val() === 'parent') {
			$('#edit-panel').addClass('edit-parent-node');
			$('#new-nodelist').children(':gt(0)').remove();
		} else {
			$('#edit-panel').removeClass('edit-parent-node');
		}
	});

	$('#btn-remove-input').on('click', function() {
		var inputs = $('#new-nodelist').children('li');
		if (inputs.length > 1) {
			inputs.last().remove();
		}
	});

	$('#btn-add-nodes').on('click', function() {
		var $chartContainer = $('#chart-container'),
			newPosition,
			val = $('#new-node-employee').val(),
			item = val != null ? employees.find(r => r.id == val) : null;
		if (item)
			newPosition = { 'name': item.title, 'title': $('#new-node-position').val(), signingAuthority: $('#new-node-signing-authority').is(":checked"), 'employee': item };

		var chartIsEmpty = !$chartContainer.children('.orgchart').find('.node').length,
			$node = $('#selected-node').data('node');

		if (!$node && !chartIsEmpty) {
			alert('Please specify position manager');
			return;
		}

		if (!newPosition) {
			alert('Please input value for new node');
			return;
		}

		var nodeType = chartIsEmpty ? 'parent' : 'child';

		if (!chartIsEmpty)
			newPosition.managerId = $node[0].id;
		else
		{
			if (!newPosition.title)
				newPosition.title = 'CEO';
		}

		if (!newPosition.title)
		{
			alert('Please input position name');
			return;
		}

		if (isOwnManager(getAllPositions(chartData), newPosition))
		{
			alert('Employee cannot be his own manager');
			return;
		}

		rest.addPosition(newPosition)
			.done(data => {
				var nodeVals = [makePosition(data)];

				if (nodeType === 'parent') {
					if (!$chartContainer.children('.orgchart').length) { // if the original chart has been deleted
						oc = loadChartView(data);
						$(oc.chart).addClass('view-state');
					} else {
						oc.addParent($chartContainer.find('.node:first'), nodeVals[0]);
					}
				} else if (nodeType === 'siblings') {
					if ($node[0].id === oc.$chart.find('.node:first')[0].id) {
						alert('You are not allowed to directly add sibling nodes to root node');
						return;
					}
					oc.addSiblings($node.get(0), { 'siblings': [$.extend(nodeVals[0], { 'relationship': '110' })] });
				} else {

					var manager = getAllPositions(chartData)[nodeVals[0].managerId];

					if (!manager.children)
						manager.children = [];
					manager.subordinates.push(nodeVals[0]);

					var hasChild = $node.parent().attr('colspan') > 0 ? true : false;

					if (!hasChild) {
						var rel = nodeVals.length > 1 ? '110' : '100';
						oc.addChildren($node.get(0),
							{ 'children': nodeVals.map(function (item) {
								return $.extend(nodeVals[0], { 'relationship': rel });
							})});
					} else {
						oc.addSiblings($node.closest('tr').siblings('.nodes').find('.node:first').get(0),
							{ 'siblings': nodeVals.map(function (item) {
									return $.extend(nodeVals[0], { 'relationship': '110' });
							})});
					}
				}
			})
			.fail((xhr, textStatus, errorThrown) => alert(textStatus));
	});

	$('#btn-delete-nodes').on('click', function() {
		var $node = $('#selected-node').data('node');
		if (!$node) {
			alert('Please select one node in orgchart');
			return;
		} else if ($node[0] === $('.orgchart').find('.node:first')[0]) {
			if (!window.confirm('Are you sure you want to delete the whole chart?'))
				return;
		}
		rest.deletePosition($node[0].id)
			.done(data => {
				oc.removeNodes($node.get(0));
				$('#selected-node').text('').data('node', null);
				clearFilterResult();
			})
			.fail((xhr, textStatus, errorThrown) => alert(textStatus));
	});
}

function getAllPositions(data, res)
{
	if (!data) return {};
	res = res || {};
	res[data.id] = data;
	if (data.subordinates)
		for (var i = 0; i < data.subordinates.length; i++)
			getAllPositions(data.subordinates[i], res);
	return res;
}

function isOwnManager(data, position)
{
	var managerId = position.managerId;
	while (managerId != null)
	{
		if (position.employee.id == data[managerId].employee.id)
			return true;
		managerId = data[managerId].managerId;
	}
	return false;
}

export function populateNewNodeSelect(newEmployee)
{
	var selects = $('#new-node-employee');
	if (newEmployee)
	{
		for (var i = 0; i < selects.length; i++)
			selects.eq(i).append('<option value=' + newEmployee.id + '>' + newEmployee.title + '</option>');
	}
	else
	{
		for (var i = 0; i < selects.length; i++)
			if (selects.eq(i).find('option').length == 0)
				for (var j = 0; j < employees.length; j++)
					selects.eq(i).append('<option value=' + employees[j].id + '>' + employees[j].title + '</option>');
	}
}
