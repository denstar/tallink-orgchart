import { initOrgChartView } from './js/org-chart-view.js';
import { populateNewNodeSelect } from './js/edit-chart-view.js';
import { initFilterNodeView } from './js/filter-node-view.js';
import { initAddEmployeeView } from './js/add-employee-view.js';

import * as rest from './js/orgchart-rest-api.js';

$(function()
{
	initFilterNodeView();
	initAddEmployeeView();

	rest.getAllEmployees()
		.done(data => { employees = data; populateNewNodeSelect() });

	rest.getOrgChart()
		.done(initOrgChartView);
});
