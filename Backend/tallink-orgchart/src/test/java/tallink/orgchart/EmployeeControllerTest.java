package tallink.orgchart;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import tallink.orgchart.Application;
import tallink.orgchart.models.Employee;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class EmployeeControllerTest extends RestControllerTestBase {

    private List<Employee> employeeList = new ArrayList<>();

    @Override
	@Before
    public void setup() throws Exception {
    	super.setup();
    
        employeeList.addAll(CreateEmployees());        
    }
    
	@Test
    public void getAllEmployees() throws Exception {
        mockMvc.perform(get("/employee/"))
	    	.andExpect(status().isOk())
	        .andExpect(content().contentType(jsonContentType))
	        .andExpect(jsonPath("$", hasSize(employeeList.size())));
    }

    @Test
    public void getEmployee() throws Exception {
        mockMvc.perform(get("/employee/" + employeeList.get(0).getId()))
	    	.andExpect(status().isOk())
	        .andExpect(content().contentType(jsonContentType))
	        .andExpect(jsonPath("$.id", is(employeeList.get(0).getId().intValue())));

        mockMvc.perform(get("/employee/" + 123456789))
	    	.andExpect(status().isNotFound());
    }

    @Test
    public void addEmployee() throws Exception {

    	Employee employee = new Employee("Daryl Warner");
    	Employee employee1 = new Employee("Daryl Warner");
    	Employee employee2 = new Employee(" Daryl Warner");
    	Employee employee3 = new Employee("daryl Warner");
    	Employee employee4 = new Employee(" daryl    warner ");

    	mockMvc.perform(post("/employee/")
            .contentType(jsonContentType)
            .content(json(employee)))
	        .andExpect(content().contentType(jsonContentType))
	    	.andExpect(jsonPath("$.title", is(employee.getTitle())));

    	mockMvc.perform(post("/employee/")
                .contentType(jsonContentType)
                .content(json(employee)))
        		.andExpect(status().is4xxClientError());

    	mockMvc.perform(post("/employee/")
                .contentType(jsonContentType)
                .content(json(employee1)))
        		.andExpect(status().is4xxClientError());

    	mockMvc.perform(post("/employee/")
            .contentType(jsonContentType)
            .content(json(employee2)))
    		.andExpect(status().is4xxClientError());

    	mockMvc.perform(post("/employee/")
            .contentType(jsonContentType)
            .content(json(employee3)))
    		.andExpect(status().is4xxClientError());

    	mockMvc.perform(post("/employee/")
            .contentType(jsonContentType)
            .content(json(employee4)))
    		.andExpect(status().is4xxClientError());
    }

    @Test
    public void deleteEmployee() throws Exception {
        mockMvc.perform(delete("/employee/" + employeeList.get(0).getId()))
        	.andExpect(status().isOk());

        mockMvc.perform(delete("/employee/" + employeeList.get(0).getId()))
	    	.andExpect(status().isNotFound());
    }
}
