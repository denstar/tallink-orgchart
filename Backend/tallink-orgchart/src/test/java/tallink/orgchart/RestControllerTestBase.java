package tallink.orgchart;

import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import tallink.orgchart.models.Company;
import tallink.orgchart.models.Employee;
import tallink.orgchart.repositories.*;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

public class RestControllerTestBase {

	protected static final Logger logger = LoggerFactory.getLogger(RestControllerTestBase.class);

	protected MediaType jsonContentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

	protected MediaType textContentType = new MediaType(MediaType.TEXT_PLAIN.getType(),
            MediaType.TEXT_PLAIN.getSubtype(),
            Charset.forName("utf8"));

	protected MockMvc mockMvc;

	protected HttpMessageConverter mappingJackson2HttpMessageConverter;

    @Autowired
    protected WebApplicationContext webApplicationContext;

    @Autowired
    protected CompanyRepository companyRepository;

    @Autowired
    protected EmployeeRepository employeeRepository;

    @Autowired
    protected PositionRepository positionRepository;

    protected Company company;
    
    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
            .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
            .findAny()
            .orElse(null);

        assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();

        this.companyRepository.deleteAllInBatch();
        this.positionRepository.deleteAllInBatch();
        this.employeeRepository.deleteAllInBatch();

        company = companyRepository.save(new Company(null));
    }
    
    protected Collection<Employee> CreateEmployees() {
        List<Employee> list = new ArrayList<>();
    	
        list.add(employeeRepository.save(new Employee("Dan Sanders")));
        list.add(employeeRepository.save(new Employee("Ralph Battle")));
        list.add(employeeRepository.save(new Employee("Karl Knox")));
        
        return list;
	}

    protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }
}
