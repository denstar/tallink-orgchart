package tallink.orgchart;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MvcResult;

import tallink.orgchart.Application;
import tallink.orgchart.exceptions.ItemNotFoundException;
import tallink.orgchart.models.Company;
import tallink.orgchart.models.Employee;
import tallink.orgchart.models.Position;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class PositionControllerTest extends RestControllerTestBase {

    private List<Employee> employeeList = new ArrayList<>();
    private List<Position> positionList = new ArrayList<>();

    @Before
    public void setup() throws Exception {
    	super.setup();

        employeeList.addAll(CreateEmployees());
        
        Position ceo = positionRepository.save(new Position(employeeList.get(0), "CEO", true));
        positionList.add(ceo);
        company.setCeo(ceo);
        companyRepository.save(company);
        
        for (int i = 1; i < employeeList.size(); i++) {
        	Position pos = new Position(employeeList.get(i), "Manager #" + i, false);
        	pos.setManager(ceo);
        	positionList.add(positionRepository.save(pos));
		}
    }

    @Test
    public void addPosition() throws Exception {

    	Employee newEmployee = employeeRepository.save(new Employee("Alejandro McCormick"));
    	employeeList.add(newEmployee);
    	
    	Position newPosition = new Position(newEmployee, "Manager #" + positionList.size(), false);
    	Position newPositionNoEmployee = new Position(null, "Manager #" + positionList.size(), false);
    	Position newPositionNoTitle = new Position(newEmployee, null, false);

    	// Success
    	mockMvc.perform(post("/position/" + positionList.get(0).getId())
            .contentType(jsonContentType)
            .content(json(newPosition)))
	    	.andExpect(status().isOk())
	        .andExpect(content().contentType(jsonContentType))
	        .andExpect(jsonPath("$.title", is(newPosition.getTitle())));
    	
    	// Error: Employee record not assigned
    	mockMvc.perform(post("/position/" + positionList.get(0).getId())
                .contentType(jsonContentType)
                .content(json(newPositionNoEmployee)))
    	    	.andExpect(status().is4xxClientError());

    	// Error: Position title not specified
    	mockMvc.perform(post("/position/" + positionList.get(0).getId())
                .contentType(jsonContentType)
                .content(json(newPositionNoTitle)))
    	    	.andExpect(status().is4xxClientError());

    	// Error: Manager record not found
    	mockMvc.perform(post("/position/123456789")
                .contentType(jsonContentType)
                .content(json(newPositionNoTitle)))
    	    	.andExpect(status().is4xxClientError());
    }

    @Test
    public void deletePosition() throws Exception {
    	
    	// Success
        mockMvc.perform(delete("/position/" + positionList.get(1).getId()))
	    	.andExpect(status().isOk());

    	// Error: Position not found
        mockMvc.perform(delete("/position/" + positionList.get(1).getId()))
	    	.andExpect(status().isNotFound());
    }
    
    @Test
    public void getCeo() throws Exception {
    	
    	// Success
    	MvcResult result = mockMvc.perform(get("/position/ceo/"))
        	.andExpect(status().isOk())
            .andExpect(content().contentType(jsonContentType))
        	.andExpect(jsonPath("$.id", is(positionList.get(0).getId().intValue())))
        	.andReturn();
    	
    	result.getResponse().getContentAsString().equals(json(positionList.get(0)));
    }

    @Test
    public void setCeo() throws Exception {
    	
    	Employee newEmployee = employeeRepository.save(new Employee("Alejandro McCormick"));    	
    	Position newPosition = new Position(newEmployee, "Manager #" + positionList.size(), false);
    	
    	// Error: Hierarchy is not empty
        mockMvc.perform(post("/position/ceo/")
            .contentType(jsonContentType)
            .content(json(newPosition)))
        	.andExpect(status().is4xxClientError());

		Company company = companyRepository.findAll().stream().findFirst().orElse(null);
		company.setCeo(null);
		companyRepository.save(company);

        this.positionRepository.deleteAllInBatch();

    	// Success
        mockMvc.perform(post("/position/ceo/")
            .contentType(jsonContentType)
            .content(json(newPosition)))
        	.andExpect(status().isOk());
    }
}
