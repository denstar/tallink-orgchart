package tallink.orgchart.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import tallink.orgchart.Application;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ItemNotFoundException extends RuntimeException {

	private static final Logger logger = LoggerFactory.getLogger(Application.class);

	private static final long serialVersionUID = 1L;

	public ItemNotFoundException(Object obj) {
		super("Could not find requested item '" + obj.toString() + "'.");
		
		logger.error("Item not found!", this);
	}

	public ItemNotFoundException(String message) {
		super(message);

		logger.error(message, this);
	}
}
