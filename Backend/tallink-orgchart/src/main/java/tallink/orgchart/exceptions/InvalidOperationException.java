package tallink.orgchart.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import tallink.orgchart.Application;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class InvalidOperationException extends RuntimeException {

	private static final Logger logger = LoggerFactory.getLogger(Application.class);

	private static final long serialVersionUID = 1L;

	public InvalidOperationException(String message) {
		super(message);

		logger.error(message, this);
	}
}