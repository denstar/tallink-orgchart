package tallink.orgchart.models;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.PostLoad;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Position {

	@Id
	@GeneratedValue
	private Long id;

	private String title;

    @OneToOne(mappedBy = "ceo")
    private Company company;
	
    @ManyToOne
	private Employee employee;

    @OneToMany(mappedBy = "manager")
    @OrderBy("title ASC")
    private Set<Position> subordinates = new HashSet<>();

    @ManyToOne
    @JsonIgnore
    private Position manager;

	private Boolean signingAuthority;

    private Position() { }

	public Position(Employee employee, String title, Boolean signingAuthority) {
		this.employee = employee;
		this.title = title;
		this.signingAuthority = signingAuthority;
	}

	public Position(String title) {
		this.title = title;
	}

    @PostLoad
    protected void normalize() {
        if (title != null)
        	title = title.trim().replaceAll("\\s+", " ");
    }
	
	public Employee getEmployee() {
		return employee;
	}

	public Long getId() {
		return id;
	}

	public Position getManager() {
		return manager;
	}

	@JsonGetter("managerId")
	public Long getManagerId() {
		return manager == null ? null : manager.getId();
	}

	public Boolean getSigningAuthority() {
		return signingAuthority;
	}

	public Set<Position> getSubordinates() {
		return subordinates;
	}

	public String getTitle() {
		return title;
	}

    public void setEmployee(Employee employee) {
    	this.employee = employee;
	}

    public void setManager(Position manager) {
		this.manager = manager;
	}

	@Override
	public String toString() {
		return "Position [id=" + id + ", title=" + title + ", signingAuthority=" + signingAuthority + "]";
	}
}
