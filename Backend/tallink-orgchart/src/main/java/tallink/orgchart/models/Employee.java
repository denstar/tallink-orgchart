package tallink.orgchart.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PostLoad;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Employee {

	@Id
	@GeneratedValue
	private Long id;
	
	private String title;

    @OneToMany(mappedBy = "employee")
    @JsonIgnore
    private Set<Position> positions = new HashSet<>();

    
	private Employee() { }
	
	public Employee(String title) {
		this.title = title;
	}

    @PostLoad
    protected void normalize() {
        if (title != null)
        	title = title.trim().replaceAll("\\s+", " ");
    }
	
	public Long getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

    public Set<Position> getPositions() {
        return positions;
    }

    @JsonGetter("positionsCount")
    public int getPositionsCount() {
        return positions.size();
    }

	@Override
	public String toString() {
		return "Employee [id=" + id + ", title=" + title + "]";
	}
}
