package tallink.orgchart.models;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Company {

    @Id
    @GeneratedValue
    private Long id;

    @OneToOne(optional = true)
    @JsonIgnore
    private Position ceo;

    private Company() { }
    
    public Company(Position ceo) {
    	this.ceo = ceo;
    }
    
	public Position getCeo() {
		return ceo;
	}

	public Long getId() {
        return id;
    }

	public void setCeo(Position ceo) {
		this.ceo = ceo;
	}
	
    @JsonGetter("ceoId")
	public Long getCeoId() {
		return ceo != null ? ceo.getId() : null;
	}

	@Override
	public String toString() {
		return "Company [id=" + id + ", ceoId=" + getCeoId() + "]";
	}
}