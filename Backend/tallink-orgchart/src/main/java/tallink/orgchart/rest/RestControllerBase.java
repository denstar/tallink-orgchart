package tallink.orgchart.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import tallink.orgchart.repositories.CompanyRepository;
import tallink.orgchart.repositories.EmployeeRepository;
import tallink.orgchart.repositories.PositionRepository;

class RestControllerBase {

	protected static final Logger logger = LoggerFactory.getLogger(RestControllerBase.class);

	@Autowired
	protected CompanyRepository companyRepository;
	@Autowired
	protected EmployeeRepository employeeRepository;
	@Autowired
	protected PositionRepository positionRepository;
}
