package tallink.orgchart.rest;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import tallink.orgchart.exceptions.InvalidArgumentException;
import tallink.orgchart.exceptions.InvalidOperationException;
import tallink.orgchart.exceptions.ItemNotFoundException;
import tallink.orgchart.models.Employee;

@RestController
@RequestMapping("/employee")
class EmployeeRestController extends RestControllerBase {

	@RequestMapping(method = RequestMethod.GET, value = "/")
	Collection<Employee> getAllEmployees() {
		return employeeRepository.findAll();
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	Employee getEmployee(@PathVariable Long id) {
		Employee employee = employeeRepository.findOne(id);
		
		if (employee == null)
			throw new ItemNotFoundException(id);
		
		return employee;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/")
	Employee addEmployee(HttpServletRequest request, @RequestBody Employee input) {

		if (input.getTitle() == null || input.getTitle().isEmpty())
			throw new InvalidArgumentException("Employee title cannot be empty!");

		if (employeeRepository.findByTitleIgnoreCase(input.getTitle().trim().replaceAll("\\s+", " ")) != null)
			throw new InvalidOperationException("Employee with same title already exists!");
		
		Employee employee = employeeRepository.save(input);

		return employee;
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	void deleteEmployee(@PathVariable("id") Long id) {

		Employee employee = employeeRepository.findOne(id);
		
		if (employee == null)
			throw new ItemNotFoundException(id);

		employeeRepository.delete(employee);
	}
}
