package tallink.orgchart.rest;

import java.util.Set;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import tallink.orgchart.exceptions.InvalidOperationException;
import tallink.orgchart.exceptions.ItemNotFoundException;
import tallink.orgchart.models.Company;
import tallink.orgchart.models.Position;

@RestController
@RequestMapping("/position")
class PositionRestController extends RestControllerBase {

	@RequestMapping(method = RequestMethod.POST, value = "/{id}")
	Position addPosition(@RequestBody Position input, @PathVariable("id") Long managerId) {

		if (input.getTitle() == null || input.getTitle().isEmpty())
			throw new InvalidOperationException("Position title not specified");

		if (input.getEmployee() == null)
			throw new InvalidOperationException("Employee record not assigned");
		
		Position manager = positionRepository.findOne(managerId);

		if (manager == null)
			throw new InvalidOperationException("Manager record not found");

		input.setManager(manager);
		
		Position result = positionRepository.save(input);

		return result;
    }

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	void deletePosition(@PathVariable("id") Long id) {

		Position position = positionRepository.findOne(id);
				
		if (position == null)
			throw new ItemNotFoundException(id);

		Company company = companyRepository.findAll().stream().findFirst().orElse(null);
		Position ceo = company != null ? company.getCeo() : null;
		
		if (ceo.getId().equals(position.getId()))
		{
			company.setCeo(null);
			companyRepository.save(company);
		}

		deleteRecursive(position);
	}

	private void deleteRecursive(Position position) {
		
		Set<Position> subordinates = position.getSubordinates();
		
		for(Position subordinate : subordinates)
			deleteRecursive(subordinate);

		positionRepository.delete(position);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/ceo")
	Position getCeo() {
		
		Company company = companyRepository.findAll().stream().findFirst().orElse(null);

		if (company == null)
			throw new ItemNotFoundException("Company record not found!");
		
		Position ceo = company.getCeo();

		return ceo;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/ceo")
	Position setCeo(@RequestBody Position input) {

		Company company = companyRepository.findAll().stream().findFirst().orElse(null);
		
		if (positionRepository.findAll().size() != 0)
			throw new InvalidOperationException("Organizational hierarchy is not empty");

		if (company == null)
			throw new ItemNotFoundException("Company record not found");

		Position position = positionRepository.save(input);

		company.setCeo(position);
		companyRepository.save(company);
		
		return position;
	}
}
