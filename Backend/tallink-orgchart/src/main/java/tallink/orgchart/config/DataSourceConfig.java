package tallink.orgchart.config;

import org.h2.jdbcx.JdbcDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import javax.sql.DataSource;

@Configuration
public class DataSourceConfig {

	@Value("${spring.application.name}")
	private String appName;

	private static final String DATA_SOURCE_NAME = "4447f5137b5e.data";
	private static final String DB_DIRECTORY = GetDbDir();
	private static final Logger logger = LoggerFactory.getLogger(DataSourceConfig.class);

	@Bean
	public DataSource createMainDataSource() {
		JdbcDataSource ds = new JdbcDataSource();
		String connStr = "jdbc:h2:" + DB_DIRECTORY + "/" + DATA_SOURCE_NAME + "." + appName + ";MODE=MySQL";

		logger.debug("Connection string: " + connStr);

		ds.setURL(connStr);

		return ds;
	}

	private static String GetDbDir() {
		return System.getProperty("java.io.tmpdir");
	}
}