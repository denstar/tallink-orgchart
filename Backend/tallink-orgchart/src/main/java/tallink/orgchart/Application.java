package tallink.orgchart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import tallink.orgchart.models.*;
import tallink.orgchart.repositories.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class Application {

	private static final Logger logger = LoggerFactory.getLogger(Application.class);
	
	public static void main(String[] args) {
		
		logger.debug("--Application Started--");
		
		SpringApplication.run(Application.class, args);
	}

	@Autowired
	private CompanyRepository companyRepository;
	@Autowired
	private EmployeeRepository employeeRepository;
	@Autowired
	private PositionRepository positionRepository;

	
	@Bean
	CommandLineRunner init() {
		return (evt) ->
		{			
			Position ceo = save("Lao Lao", "CEO", true, new HashSet<Position>() {{
				add(save("Bo Miao", "Department manager", true));
				add(save("Su Miao", "Department manager", true, new HashSet<Position>() {{
					add(save("Tie Hua", "Senior engineer"));
					add(save("Hei Hei", "Senior engineer", false, new HashSet<Position>() {{
							add(save("Pang Pang", "Engineer"));
							add(save("Xiang Xiang", "UE engineer"));
					}}));
				}}));					
				add(save("Yu Jie", "Department manager"));
				add(save("Yu Li", "Department manager"));
				add(save("Hong Miao", "Department manager"));
				add(save("Yu Wei", "Department manager"));
				add(save("Chun Miao", "Department manager"));
				add(save("Yu Tie", "Department manager"));
			}});
			
			save(new Company(ceo));
		};
	}

	private Company save(Company company) {
		return companyRepository.save(company);
	}

	private Position save(Position position) {
		return positionRepository.save(position);
	}

	private Position save(Position position, Set<Position> subordinates) {
		
		position = positionRepository.save(position);
		
		if (subordinates != null)
			for (Position subordinate : subordinates) {
				subordinate.setManager(position);
				save(subordinate);
			}
		
		return position;
	}

	private Position save(String name, String positionName) {
		return save(new Position(save(new Employee(name)), positionName, false));
	}

	private Position save(String name, String positionName, boolean signingAuthority) {
		return save(new Position(save(new Employee(name)), positionName, signingAuthority));
	}

	private Position save(String name, String positionName, boolean signingAuthority, Set<Position> subordinates) {
		return save(new Position(save(new Employee(name)), positionName, signingAuthority), subordinates);
	}

	private Employee save(Employee employee) {
		return employeeRepository.save(employee);
	}

	@Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
            	registry.addMapping("/**")
	                .allowedOrigins("*")
	                .allowedMethods("OPTIONS", "HEAD", "GET", "PUT", "POST", "DELETE", "PATCH");
            }
        };
    }
	
}
// end::runner[]

