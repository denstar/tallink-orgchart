package tallink.orgchart.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import tallink.orgchart.models.Company;
import tallink.orgchart.models.Employee;

import java.util.Optional;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
	Employee findByTitleIgnoreCase(String title);
}
