package tallink.orgchart.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import tallink.orgchart.models.Company;
import tallink.orgchart.models.Position;

import java.util.Optional;

public interface PositionRepository extends JpaRepository<Position, Long> {
}
