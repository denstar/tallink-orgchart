package tallink.orgchart.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import tallink.orgchart.models.Company;

import java.util.Optional;

public interface CompanyRepository extends JpaRepository<Company, Long> {
}
