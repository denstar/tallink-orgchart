# Installation and Build

## Back-end

	cd "[backand folder]"
	mvn install
	mvn package

NB! Running "mvn install" is required only first time

## Front-end

	cd "[frontend folder]"
	npm install
	gulp build

#Testing

## Back-end

	cd "[backand folder]"
	mvn test

## Front-end

	cd "[frontend folder]"
	gulp tests

Tests also run automatically before "gulp serve" starts

# Running solution

## Back-end

	cd "[backand folder]"
	cd "Backend\tallink-orgchart\target"
	java -jar tallink-orgchart-1.0.0.jar

Backend is expected to run on http://localhost:8080.
It may be configured using settings.urls.endpoint value in "[frontend folder]\src\js\settings.js"

## Front-end

	cd "[frontend folder]"
	gulp serve

By default frontend will run on http://localhost:3000.

# Implementation details
## Non-functional requirements
* Frontend: Front-end should be built using Ecmascript 6 and in functional style
* Backend: Application should be built using in back-end one of programming languages, which is running in JVM (e.g. Java, Scala, Kotlin etc)
* DB: Datastore for structure model should be designed and decided should be decided by candidate
* Tests: Application should be covered with tests
* Docs: building procedure should be added into project as README.MD
## Data
* Employee can have more than one manager
* One person in structure won't have top managers (CEO)
## Functional requirements (primary)
* View hierarchical structure in visual way. (showing all employees)
* Search in visual structure by name and highlight found persons
* Add under found employee newly added employee
	* After searching existing employees in hierarchy
* Delete position in org. chart
## Delivery
* Generate sample data
* Working solution should be presented on local machine
* Publish code in Github or BitBucket and share link
## Requirements (optional)
* Functional
	* Show another visual structure with the same employees, but with possibility to show another details: "Who can sign important documents in hierarchy"
* Delivery
	* Deploy solution in free cloud to demo (Amazon AWS , Azure)

# Data model diagram
![alt text](data-model.png "Data model diagram")

# Implemented validation rules

**Adding employee**

* Verify that there are no employees with the same name in database

**Adding position**

* Verify that employee name and manager is specified
	* If org chart is empty manager and position title may be empty. If position title not specified it will be set to "CEO"
* Verify that employee assuming new position would not become his own manager

# Searching
It is possible to search for positions with "signing authority". To do this enter in the search field: "sign:yes"

# References
[Organization chart plugin](https://github.com/dabeng/OrgChart)
