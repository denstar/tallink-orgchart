module.exports = function(ops)
{
	var ops = ops || {};

	this.getAllEmployees = ajaxCallMock(function()
	{
		return [];
	});

	this.getOrgChart = ajaxCallMock(function()
	{
		return [];
	});

	this.addPosition = ajaxCallMock(function(position)
	{
		return position;
	});

	this.deletePosition = ajaxCallMock(function(positionId)
	{
	});

	this.publishEmployee = ajaxCallMock(function(employee)
	{
		return employee;
	});

	function ajaxCallMock(donePreviewer, failPreviewer)
	{
		var self = this,
			args, doneFn, failFn;

		function run()
		{
			doneFn(donePreviewer.apply(self, args));
		}

		function o() { args = [arguments[0]]; return o; }

		o.done = function(_doneFn) { doneFn = _doneFn; return o; }
		o.fail = function(_failFn) { failFn = _failFn; run() }

		return o;
	}
}
