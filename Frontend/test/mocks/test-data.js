module.exports = {
	testData: {
    'id': 'n1',
    'title': '',
    signingAuthority: true,
    employee: { 'title': 'Lao Lao', id: 'e1' },
    'children': [
      { 'id': 'n2', 'title': '', employee: { 'title': 'Bo Miao' }, managerId: 'n1', signingAuthority: true },
      { 'id': 'n3', 'title': '', employee: { 'title': 'Su Miao' }, managerId: 'n1',
        'children': [
          { 'id': 'n5', 'title': '', employee: { 'title': 'Tie Hua' }, managerId: 'n3',
            'children' : [
              { 'id': 'n8', 'title': '', employee: { 'title': 'Dan Dan', id: 'e8' }, managerId: 'n5' }
            ]
          },
          { 'id': 'n6', 'title': '', employee: { 'title': 'Hei Hei' }, managerId: 'n3',
            'children': [
              { 'id': 'n9', 'title': '', employee: { 'title': 'Er Dan' }, managerId: 'n6' }
            ]
          },
          { 'id': 'n7', 'title': '', employee: { 'title': 'Pang Pang' }, managerId: 'n3',
            'children': [
              { 'id': 'n10', 'title': '', employee: { 'title': 'San Dan' }, managerId: 'n7', signingAuthority: true }
            ]
          }
        ]
      },
      { 'id': 'n4', 'title': '', employee: { 'title': 'Hong Miao' } },
    ]
  },
  hierarchy: {
    id: 'n1',
    children: [
      { id: 'n2' },
      { id: 'n3',
        children: [
          { id: 'n5',
            children: [
              { id: 'n8' }
            ]
          },
          { id: 'n6',
            children: [
              { id: 'n9'}
            ]
          },
          { id: 'n7',
            children: [
              { id: 'n10' }
            ]
          }
        ]
      },
      { id: 'n4' }
    ]
  }
}