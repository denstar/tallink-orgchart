require('app-module-path').addPath(__dirname + '/../../src/js/');
var chai = require('chai');
var sinon = require('sinon');
var sinonChai = require('sinon-chai');
var should = chai.should();
chai.use(sinonChai);
require('jsdom-global')();
var $ = require('jquery');
require('../../assets/orgchart/js/jquery.orgchart');
var initOrgChartExtension = require('org-chart-extension').initOrgChartExtension;
var loadOrgChart = require('org-chart-view').loadOrgChart;
var initAddEmployeeView = require('add-employee-view').initAddEmployeeView;
var testData = require('../mocks/test-data');
var OrgchartBackendMock = require('../mocks/rest-api');
var popupMock = require('../mocks/popup');

describe('orgchart -- integration tests', function () {
  document.body.innerHTML = '<div id="chart-container"></div>';
  var $container = $('#chart-container'),
  ds = testData.testData,
  oc = {},
  state = { employees: [] },
  eventSink = { newEmployee: function() { } };

  initOrgChartExtension('.orgchart', eventSink, state);
  var orgchartBackendMock = new OrgchartBackendMock(),
	  addEmployeeView = initAddEmployeeView(eventSink, state, orgchartBackendMock, popupMock);

  beforeEach(function () {
    oc = loadOrgChart(ds);
  });
    
  afterEach(function () {
    $container.empty();
  });

  it('add employee - validation', function()
  {
	  var alertSpy = sinon.spy(popupMock, 'alert');
	  addEmployeeView.addEmployee({});
	  alertSpy.should.have.been.callCount(1);
      alertSpy.should.have.been.calledWith('Please input name for the new employee');
	  alertSpy.restore();
  });

  it('add employee', function()
  {
	  var api_publishEmployeeSpy = sinon.spy(orgchartBackendMock, 'publishEmployee');
	  var eventSink_newEmployeeSpy = sinon.spy(eventSink, 'newEmployee');

      state.employees = [];

	  addEmployeeView.addEmployee({ title: 'Matthew Robbins' });

	  api_publishEmployeeSpy.should.have.been.callCount(1);
	  eventSink_newEmployeeSpy.should.have.been.callCount(1);
	  state.employees.length.should.equal(1);
  });
});
