require('app-module-path').addPath(__dirname + '/../../src/js/');
var chai = require('chai');
var sinon = require('sinon');
var sinonChai = require('sinon-chai');
var should = chai.should();
chai.use(sinonChai);
require('jsdom-global')();
var $ = require('jquery');
require('../../assets/orgchart/js/jquery.orgchart');
var initOrgChartExtension = require('org-chart-extension').initOrgChartExtension;
var loadOrgChart = require('org-chart-view').loadOrgChart;
var testData = require('../mocks/test-data');

describe('orgchart -- integration tests', function () {
  document.body.innerHTML = '<div id="chart-container"></div>';
  var $container = $('#chart-container'),
  ds = testData.testData,
  oc = {},
  $laolao,
  $bomiao,
  $sumiao,
  $hongmiao,
  $chunmiao,
  $tiehua,
  $heihei,
  $pangpang,
  $dandan,
  $erdan,
  allNodes = [$laolao, $bomiao, $sumiao, $hongmiao, $chunmiao, $tiehua, $heihei, $pangpang, $dandan, $erdan];
  state = {},
  eventSink = {};

  initOrgChartExtension('.orgchart', eventSink, state);

  beforeEach(function () {
    oc = loadOrgChart(ds),
    $laolao = $('#n1'),
    $bomiao = $('#n2'),
    $sumiao = $('#n3'),
    $hongmiao = $('#n4'),
    $tiehua = $('#n5'),
    $heihei = $('#n6'),
    $pangpang = $('#n7'),
    $dandan = $('#n8'),
    $erdan = $('#n9'),
    $sandan = $('#n10');
  });
    
  afterEach(function () {
    $laolao = $bomiao = $sumiao = $hongmiao = $chunmiao = $tiehua = $heihei = $pangpang = $dandan = $erdan = $sandan = null;
    $container.empty();
  });

  it('filter nodes: Lao', function()
  {
    eventSink.filterNodes('Lao');
    $('.orgchart').find('.matched').should.lengthOf(1);
	$('.orgchart').find('.matched')[0].id.should.equal('n1');
  });

  it('filter nodes: ao', function()
  {
    eventSink.filterNodes('ao');
    $('.orgchart').find('.matched').should.lengthOf(4);
  });

  it('filter nodes: can sign', function()
  {
    eventSink.filterNodes('sign:true');
    $('.orgchart').find('.matched').should.lengthOf(3);
  });
});
