require('app-module-path').addPath(__dirname + '/../../src/js/');
var chai = require('chai');
var sinon = require('sinon');
var sinonChai = require('sinon-chai');
var should = chai.should();
chai.use(sinonChai);
require('jsdom-global')();
var $ = require('jquery');
require('../../assets/orgchart/js/jquery.orgchart');
var initOrgChartExtension = require('org-chart-extension').initOrgChartExtension;
var initOrgChartView = require('org-chart-view').initOrgChartView;
var initEditChartView = require('edit-chart-view').initEditChartView;
var testData = require('../mocks/test-data');
var OrgchartBackendMock = require('../mocks/rest-api');
var popupMock = require('../mocks/popup');

describe('orgchart -- integration tests', function () {
  document.body.innerHTML = '<input type="text" id="selected-node"><div id="chart-container"></div>';
  var $container = $('#chart-container'),
  ds = testData.testData,
  oc = {},
  state = { employees: [] },
  eventSink = { newEmployee: _ => {}, newPosition: _ => {} };

  initOrgChartExtension('.orgchart', eventSink, state);
  var orgchartBackendMock = new OrgchartBackendMock(),
	  orgChartView = initOrgChartView(eventSink, state, orgchartBackendMock, popupMock),
	  editChartView = initEditChartView(eventSink, state, orgchartBackendMock, popupMock);

  beforeEach(function () {
    oc = orgChartView.loadChartView(ds);
  });
    
  afterEach(function () {
    $container.empty();
  });

  it('add position - validation', function()
  {
	  var alertSpy = sinon.spy(popupMock, 'alert');
	  editChartView.addPosition({ 'name': 'Name', 'title': 'Title', signingAuthority: true, 'employee': { title: 'Donnie Stanley' } });
	  alertSpy.should.have.been.callCount(1);
      alertSpy.should.have.been.calledWith('Please specify position manager');
	  alertSpy.restore();
  });

  it('add position - validation - circular reference', function()
  {
	  var alertSpy = sinon.spy(popupMock, 'alert');
	  editChartView.addPosition({ 'name': 'Name', 'title': 'Title', signingAuthority: true, 'employee': { 'title': 'Lao Lao', id: 'e1' } }, $('#n8'));
	  alertSpy.should.have.been.callCount(1);
      alertSpy.should.have.been.calledWith('Employee cannot be his own manager');
	  alertSpy.restore();
  });
});
