var addEmployee;

if (typeof module === 'object')
{
	$ = require('jquery');
	module.exports = {
		initAddEmployeeView: initAddEmployeeView
	};
}

function initAddEmployeeView(eventSink, state, api, popup)
{
	$('#btn-add-employee').on('click', function()
	{
		addEmployee({ title: $('#new-employee').val() });
	});

	function addEmployee(employee)
	{
		if (!employee.title)
		{
			popup.alert('Please input name for the new employee');
			return;
		}

		api.publishEmployee(employee)
			.done(function(data)
			{
				state.employees.push(data);
				eventSink.newEmployee(data);
				$('#new-employee').val('');
				$('#add-employee-status').text('Employee added');
				$('#add-employee-status').fadeIn().delay(500).fadeOut();
			})
			.fail( function(xhr, textStatus, errorThrown) {
				$('#add-employee-error').text(getErrorMessage(xhr));
				$('#add-employee-error').fadeIn().delay(500).fadeOut();
			});
	}

	function getErrorMessage(xhr)
	{
		try
		{
			return JSON.parse(xhr.responseText).message.substring(0, 30);
		}
		catch (e)
		{
			return ((xhr || {}).responseText || '').substring(0, 30);
		}
	}

	return { addEmployee: addEmployee }
}

if (typeof module === 'object')
{
	$ = require('jquery');
	isOwnManager = require('utils-orgchart').isOwnManager;
	getAllPositions = require('utils-orgchart').getAllPositions;
	makePosition = require('utils-orgchart').makePosition;
	module.exports = {
		initEditChartView: initEditChartView
	};
}

function initEditChartView(eventSink, state, api, popup)
{
	eventSink.orgChartCreated = function(oc)
	{
		if (!oc) return;

		oc.$chartContainer.on('click', '.node', function() {
			var $this = $(this);
			$('#selected-node').val($this.find('.title').text()).data('node', $this);
		});

		oc.$chartContainer.on('click', state.chartNode, function(event) {
			if (!$(event.target).closest('.node').length) {
				$('#selected-node').val('');
			}
		});
	}

	eventSink.employeesData = loadEmployees;

	$('input[name="chart-state"]').on('click', function() {
		$(state.chartNode).toggleClass('edit-state', this.value !== 'view');
		$('#edit-panel').toggleClass('edit-state', this.value === 'view');
		if ($(this).val() === 'edit') {
			$(state.chartNode).find('tr').removeClass('hidden')
				.find('td').removeClass('hidden')
				.find('.node').removeClass('slide-up slide-down slide-right slide-left');
		} else {
			$('#btn-reset').trigger('click');
		}
	});

	$('input[name="node-type"]').on('click', function() {
		var $this = $(this);
		if ($this.val() === 'parent') {
			$('#edit-panel').addClass('edit-parent-node');
			$('#new-nodelist').children(':gt(0)').remove();
		} else {
			$('#edit-panel').removeClass('edit-parent-node');
		}
	});

	$('#btn-remove-input').on('click', function() {
		var inputs = $('#new-nodelist').children('li');
		if (inputs.length > 1) {
			inputs.last().remove();
		}
	});

	$('#btn-add-nodes').on('click', function()
	{
		var $chartContainer = $('#chart-container'),
			newPosition,
			val = $('#new-node-employee').val(),
			item = val != null ? state.employees.find(r => r.id == val) : null;
		if (item)
			newPosition = { 'name': item.title, 'title': $('#new-node-position').val(), signingAuthority: $('#new-node-signing-authority').is(":checked"), 'employee': item };

		addPosition(newPosition, $('#selected-node').data('node'));
	});

	$('#btn-delete-nodes').on('click', function() {
		var $node = $('#selected-node').data('node');
		if (!$node) {
			popup.alert('Please select one node in orgchart');
			return;
		} else if ($node[0] === $(state.chartNode).find('.node:first')[0]) {
			if (!popup.confirm('Are you sure you want to delete the whole chart?'))
				return;
		}
		api.deletePosition($node[0].id)
			.done(data => eventSink.positionDeleted($node, data))
			.fail((xhr, textStatus, errorThrown) => popup.alert(textStatus));
	});

	$('#btn-reset').on('click', function() {
		$(state.chartNode).find('.focused').removeClass('focused');
		$('#selected-node').val('');
		$('#new-nodelist').find('input:first').val('').parent().siblings().remove();
		$('#node-type-panel').find('input').prop('checked', false);
	});

	function addPosition(newPosition, $node) {

		var $chartContainer = $('#chart-container'),
			chartIsEmpty = !$chartContainer.children(state.chartNode).find('.node').length;

		if (!$node && !chartIsEmpty) {
			popup.alert('Please specify position manager');
			return;
		}

		if (!newPosition) {
			popup.alert('Please input value for new node');
			return;
		}

		if (!chartIsEmpty)
			newPosition.managerId = $node[0].id;
		else
		{
			if (!newPosition.title)
				newPosition.title = 'CEO';
		}

		if (!newPosition.title)
		{
			popup.alert('Please input position name');
			return;
		}

		var data = ($chartContainer.children(state.chartNode).data('options') || {}).data || {};

		if (isOwnManager(getAllPositions(data), newPosition))
		{
			popup.alert('Employee cannot be his own manager');
			return;
		}

		api.addPosition(newPosition)
			.done(data => eventSink.newPosition(data, $node))
			.fail((xhr, textStatus, errorThrown) => popup.alert(textStatus));
	}

	function loadEmployees(employees)
	{
		var selects = $('#new-node-employee');

		for (var i = 0; i < selects.length; i++)
			if (selects.eq(i).find('option').length == 0)
				for (var j = 0; j < employees.length; j++)
					selects.eq(i).append('<option value=' + employees[j].id + '>' + employees[j].title + '</option>');
	}

	function newEmployee(newEmployee)
	{
		var selects = $('#new-node-employee');

		for (var i = 0; i < selects.length; i++)
			selects.eq(i).append('<option value=' + newEmployee.id + '>' + newEmployee.title + '</option>');
	}

	eventSink.newEmployee = newEmployee;

	return {
		addPosition: addPosition,
		newEmployee: newEmployee
	}
}

function initFilterNodeView(eventSink, state)
{
	$('#key-word').on('keyup', function(event) {
		if (event.which === 13) {
			eventSink.clearFilterResult();
			eventSink.filterNodes(this.value);
		}
	});

	$('#btn-filter-cancel').on('click', eventSink.clearFilterResult);
}

if (typeof module === 'object')
{
	$ = require('jquery');
	module.exports = {
		initOrgChartExtension: initOrgChartExtension
	};
}

function initOrgChartExtension(chartNode, eventSink, state, popup)
{
	state.chartNode = chartNode;

	function filterNodes(keyWord) {

		var keyWord = (keyWord || '').toLowerCase(),
			filterFn = keyWord.match(/^sign:/) ? signingAuthorityFilter : textFilter;

		if(!keyWord.length) {
			clearFilterResult();
			return;
		} else {
			var $chart = $(chartNode);
			// disalbe the expand/collapse feture
			$chart.addClass('noncollapsable');
			// distinguish the matched nodes and the unmatched nodes according to the given key word
			$chart.find('.node').filter(filterFn).addClass('matched')
				.closest('table').parents('table').find('tr:first').find('.node').addClass('retained');
			// hide the unmatched nodes
			$chart.find('.matched,.retained').each(function(index, node) {
				$(node).removeClass('slide-up')
					.closest('.nodes').removeClass('hidden')
					.siblings('.lines').removeClass('hidden');
				var $unmatched = $(node).closest('table').parent().siblings().find('.node:first:not(.matched,.retained)')
					.closest('table').parent().addClass('hidden');
				$unmatched.parent().prev().children().slice(1, $unmatched.length * 2 + 1).addClass('hidden');
			});
			// hide the redundant descendant nodes of the matched nodes
			$chart.find('.matched').each(function(index, node) {
				if (!$(node).closest('tr').siblings(':last').find('.matched').length) {
					$(node).closest('tr').siblings().addClass('hidden');
				}
			});
		}

		function signingAuthorityFilter(index, node) {
			var isSigningAuthority = !!($(node).data('item') || {}).signingAuthority;
			return keyWord.match(/^sign:(yes|true)/i) ? isSigningAuthority : !isSigningAuthority;
		}

		function textFilter(index, node) {
			var item = ($(node).data('item') || {}),
				text = (item.name || '') + ' ' + (item.title || '');
			return text.toLowerCase().indexOf(keyWord) > -1;
		}
	}

	function clearFilterResult() {
		$(chartNode).removeClass('noncollapsable')
			.find('.node').removeClass('matched retained')
			.end().find('.hidden').removeClass('hidden')
			.end().find('.slide-up, .slide-left, .slide-right, .slide-down').removeClass('slide-up slide-right slide-left slide-down');
	}

	function newPosition(data, $node)
	{
		var $chartContainer = $('#chart-container'),
			chartIsEmpty = !$chartContainer.children(state.chartNode).find('.node').length,
			nodeVals = [makePosition(data)],
			oc = state.orgChart,
			nodeType = chartIsEmpty ? 'parent' : 'child';

		if (nodeType === 'parent') {
			if (!$chartContainer.children(state.chartNode).length) { // if the original chart has been deleted
				oc = loadChartView(data);
				oc.$chart.addClass('view-state');
			} else {
				oc.addParent($chartContainer.find('.node:first'), nodeVals[0]);
			}
		} else if (nodeType === 'siblings') {
			if ($node[0].id === oc.$chart.find('.node:first')[0].id) {
				popup.alert('You are not allowed to directly add sibling nodes to root node');
				return;
			}
			oc.addSiblings($node, [$.extend(nodeVals[0], { 'relationship': '110' })]);
		} else {

			var data = ($chartContainer.children(state.chartNode).data('options') || {}).data || {},
				manager = getAllPositions(data)[nodeVals[0].managerId];

			if (!manager.children)
				manager.children = [];
			manager.children.push(nodeVals[0]);

			var hasChild = $node.parent().attr('colspan') > 0 ? true : false;

			if (!hasChild) {
				var rel = nodeVals.length > 1 ? '110' : '100';
				oc.addChildren($node, nodeVals.map(function (item) {
						return $.extend(nodeVals[0], { 'relationship': rel });
					}));
			} else {
				oc.addSiblings($node.closest('tr').siblings('.nodes').find('.node:first'), nodeVals.map(function (item) {
						return $.extend(nodeVals[0], { 'relationship': '110' });
					}));
			}
		}
	}

	function positionDeleted($node, data)
	{
		state.orgChart.removeNodes($node);
		$('#selected-node').val('').data('node', null);
		eventSink.clearFilterResult();
	}

	eventSink.newPosition = newPosition;
	eventSink.positionDeleted = positionDeleted;
	eventSink.filterNodes = filterNodes;
	eventSink.clearFilterResult = clearFilterResult;
}


var loadChartView;

if (typeof module === 'object')
{
	$ = require('jquery');
	makeChart = require('utils-orgchart').makeChart;
	module.exports = {
		initOrgChartView: initOrgChartView,
		loadOrgChart: loadOrgChart
	};
}

function initOrgChartView(eventSink, state, api)
{
	loadChartView = function(data)
	{
		state.orgChart = null;

		if (data)
			state.orgChart = loadOrgChart(data);

		eventSink.orgChartCreated(state.orgChart);

		return state.orgChart;
	}

	eventSink.orgchartData = loadChartView;

	return {
		loadChartView: loadChartView,
		loadOrgChart: loadOrgChart
	}
}

function loadOrgChart(data, nodeContent)
{
	return $('#chart-container').orgchart({
		'data' : makeChart(data),
		'nodeContent': nodeContent || 'title',
		'nodeTemplate': nodeTemplate,
		'createNode': ($nodeDiv, data) => { $nodeDiv.data('item', data) }
	});
}

function showSigningAuthorityHtml(val)
{
	return val ? '<br><b>Signing authority</b>' : '';
}

function nodeTemplate(data)
{
	var strSigningAuthority = showSigningAuthorityHtml(data['signingAuthority']),
		strStyle = data['signingAuthority'] ? ' class="content" style="height:30pt;"' : ' class="content"';

	return '<div class="title">' + data['name'] + '</div>'
		+ '<div'+ strStyle +'>' + (data['title'] || '') + strSigningAuthority + '</div>'
}

var orgchartBackend = {
	getAllEmployees: api_getAllEmployees,
	getOrgChart: api_getOrgChart,
	addPosition: api_addPosition,
	deletePosition: api_deletePosition,
	publishEmployee: api_publishEmployee
};

function api_getAllEmployees()
{
	return ajaxLogging($.ajax({
		url:settings.urls.employee
	}));
}

function api_getOrgChart()
{
	return ajaxLogging($.ajax({
		url:settings.urls.ceo
	}));
}

function api_addPosition(position)
{
	return ajaxLogging($.ajax({
		headers: { 
			'Accept': 'application/json',
			'Content-Type': 'application/json' 
		},
		type: 'POST',
		url:settings.urls.position + (position.managerId || 'ceo') +'/',
		data: JSON.stringify(position)
	}));
}

function api_deletePosition(positionId)
{
	return ajaxLogging($.ajax({
		type: 'DELETE',
		url:settings.urls.position + positionId +'/'
	}));
}

function api_publishEmployee(employee)
{
	return ajaxLogging($.ajax({
		headers: { 
			'Accept': 'application/json',
			'Content-Type': 'application/json' 
		},
		type: 'POST',
		url:settings.urls.employee,
		data: JSON.stringify(employee)
	}));
}

function ajaxLogging(ajax)
{
	return ajax
		.done(function(data)
		{
			console.log(data);
		})
		.fail((xhr, textStatus, errorThrown) =>
		{
			console.error([xhr, textStatus, errorThrown]);
		});
}

var settings = {};
	settings.urls = {};
	settings.urls.endpoint = 'http://127.0.0.1:8080';
	settings.urls.employee = settings.urls.endpoint + '/employee/';
	settings.urls.position = settings.urls.endpoint + '/position/';
	settings.urls.ceo = settings.urls.endpoint + '/position/ceo/';

if (typeof module === 'object')
{
	module.exports = {
		makeChart: makeChart,
		isOwnManager: isOwnManager,
		getAllPositions: getAllPositions,
		makePosition: makePosition
	};
}

function makeChart(obj)
{
	var CEO = obj ? makePosition(obj) : null,
		chart = CEO;

	return chart;
}

function makePosition(position)
{
	position.name = position.employee.title;

	if (position.subordinates)
	{
		position.children = position.subordinates;
		delete position.subordinates;
	}

	if (position.children)
		for (var k in position.children)
			position.children[k] = makePosition(position.children[k]);

	return position;
}

function getAllPositions(data, res)
{
	res = res || {};
	res[data.id] = data;
	if (data.children)
		for (var i = 0; i < data.children.length; i++)
			getAllPositions(data.children[i], res);
	return res;
}

function isOwnManager(data, position)
{
	var managerId = position.managerId;
	while (managerId != null)
	{
		console.log('isOwnManager.managerId: ' + managerId);
		if (position.employee.id == data[managerId].employee.id)
		{
					console.log('isOwnManager.result: ' + true);
			return true;
		}
		managerId = data[managerId].managerId;
	}
					console.log('isOwnManager.result: ' + false);
	return false;
}

//# sourceMappingURL=bundle.js.map
