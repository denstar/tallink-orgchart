var addEmployee;

if (typeof module === 'object')
{
	$ = require('jquery');
	module.exports = {
		initAddEmployeeView: initAddEmployeeView
	};
}

function initAddEmployeeView(eventSink, state, api, popup)
{
	$('#btn-add-employee').on('click', function()
	{
		addEmployee({ title: $('#new-employee').val() });
	});

	function addEmployee(employee)
	{
		if (!employee.title)
		{
			popup.alert('Please input name for the new employee');
			return;
		}

		api.publishEmployee(employee)
			.done(function(data)
			{
				state.employees.push(data);
				eventSink.newEmployee(data);
				$('#new-employee').val('');
				$('#add-employee-status').text('Employee added');
				$('#add-employee-status').fadeIn().delay(500).fadeOut();
			})
			.fail( function(xhr, textStatus, errorThrown) {
				$('#add-employee-error').text(getErrorMessage(xhr));
				$('#add-employee-error').fadeIn().delay(500).fadeOut();
			});
	}

	function getErrorMessage(xhr)
	{
		try
		{
			return JSON.parse(xhr.responseText).message.substring(0, 30);
		}
		catch (e)
		{
			return ((xhr || {}).responseText || '').substring(0, 30);
		}
	}

	return { addEmployee: addEmployee }
}
