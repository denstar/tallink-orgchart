var orgchartBackend = {
	getAllEmployees: api_getAllEmployees,
	getOrgChart: api_getOrgChart,
	addPosition: api_addPosition,
	deletePosition: api_deletePosition,
	publishEmployee: api_publishEmployee
};

function api_getAllEmployees()
{
	return ajaxLogging($.ajax({
		url:settings.urls.employee
	}));
}

function api_getOrgChart()
{
	return ajaxLogging($.ajax({
		url:settings.urls.ceo
	}));
}

function api_addPosition(position)
{
	return ajaxLogging($.ajax({
		headers: { 
			'Accept': 'application/json',
			'Content-Type': 'application/json' 
		},
		type: 'POST',
		url:settings.urls.position + (position.managerId || 'ceo') +'/',
		data: JSON.stringify(position)
	}));
}

function api_deletePosition(positionId)
{
	return ajaxLogging($.ajax({
		type: 'DELETE',
		url:settings.urls.position + positionId +'/'
	}));
}

function api_publishEmployee(employee)
{
	return ajaxLogging($.ajax({
		headers: { 
			'Accept': 'application/json',
			'Content-Type': 'application/json' 
		},
		type: 'POST',
		url:settings.urls.employee,
		data: JSON.stringify(employee)
	}));
}

function ajaxLogging(ajax)
{
	return ajax
		.done(function(data)
		{
			console.log(data);
		})
		.fail((xhr, textStatus, errorThrown) =>
		{
			console.error([xhr, textStatus, errorThrown]);
		});
}
