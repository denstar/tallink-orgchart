function initFilterNodeView(eventSink, state)
{
	$('#key-word').on('keyup', function(event) {
		if (event.which === 13) {
			eventSink.clearFilterResult();
			eventSink.filterNodes(this.value);
		}
	});

	$('#btn-filter-cancel').on('click', eventSink.clearFilterResult);
}
