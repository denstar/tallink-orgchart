if (typeof module === 'object')
{
	$ = require('jquery');
	module.exports = {
		initOrgChartExtension: initOrgChartExtension
	};
}

function initOrgChartExtension(chartNode, eventSink, state, popup)
{
	state.chartNode = chartNode;

	function filterNodes(keyWord) {

		var keyWord = (keyWord || '').toLowerCase(),
			filterFn = keyWord.match(/^sign:/) ? signingAuthorityFilter : textFilter;

		if(!keyWord.length) {
			clearFilterResult();
			return;
		} else {
			var $chart = $(chartNode);
			// disalbe the expand/collapse feture
			$chart.addClass('noncollapsable');
			// distinguish the matched nodes and the unmatched nodes according to the given key word
			$chart.find('.node').filter(filterFn).addClass('matched')
				.closest('table').parents('table').find('tr:first').find('.node').addClass('retained');
			// hide the unmatched nodes
			$chart.find('.matched,.retained').each(function(index, node) {
				$(node).removeClass('slide-up')
					.closest('.nodes').removeClass('hidden')
					.siblings('.lines').removeClass('hidden');
				var $unmatched = $(node).closest('table').parent().siblings().find('.node:first:not(.matched,.retained)')
					.closest('table').parent().addClass('hidden');
				$unmatched.parent().prev().children().slice(1, $unmatched.length * 2 + 1).addClass('hidden');
			});
			// hide the redundant descendant nodes of the matched nodes
			$chart.find('.matched').each(function(index, node) {
				if (!$(node).closest('tr').siblings(':last').find('.matched').length) {
					$(node).closest('tr').siblings().addClass('hidden');
				}
			});
		}

		function signingAuthorityFilter(index, node) {
			var isSigningAuthority = !!($(node).data('item') || {}).signingAuthority;
			return keyWord.match(/^sign:(yes|true)/i) ? isSigningAuthority : !isSigningAuthority;
		}

		function textFilter(index, node) {
			var item = ($(node).data('item') || {}),
				text = (item.name || '') + ' ' + (item.title || '');
			return text.toLowerCase().indexOf(keyWord) > -1;
		}
	}

	function clearFilterResult() {
		$(chartNode).removeClass('noncollapsable')
			.find('.node').removeClass('matched retained')
			.end().find('.hidden').removeClass('hidden')
			.end().find('.slide-up, .slide-left, .slide-right, .slide-down').removeClass('slide-up slide-right slide-left slide-down');
	}

	function newPosition(data, $node)
	{
		var $chartContainer = $('#chart-container'),
			chartIsEmpty = !$chartContainer.children(state.chartNode).find('.node').length,
			nodeVals = [makePosition(data)],
			oc = state.orgChart,
			nodeType = chartIsEmpty ? 'parent' : 'child';

		if (nodeType === 'parent') {
			if (!$chartContainer.children(state.chartNode).length) { // if the original chart has been deleted
				oc = loadChartView(data);
				oc.$chart.addClass('view-state');
			} else {
				oc.addParent($chartContainer.find('.node:first'), nodeVals[0]);
			}
		} else if (nodeType === 'siblings') {
			if ($node[0].id === oc.$chart.find('.node:first')[0].id) {
				popup.alert('You are not allowed to directly add sibling nodes to root node');
				return;
			}
			oc.addSiblings($node, [$.extend(nodeVals[0], { 'relationship': '110' })]);
		} else {

			var data = ($chartContainer.children(state.chartNode).data('options') || {}).data || {},
				manager = getAllPositions(data)[nodeVals[0].managerId];

			if (!manager.children)
				manager.children = [];
			manager.children.push(nodeVals[0]);

			var hasChild = $node.parent().attr('colspan') > 0 ? true : false;

			if (!hasChild) {
				var rel = nodeVals.length > 1 ? '110' : '100';
				oc.addChildren($node, nodeVals.map(function (item) {
						return $.extend(nodeVals[0], { 'relationship': rel });
					}));
			} else {
				oc.addSiblings($node.closest('tr').siblings('.nodes').find('.node:first'), nodeVals.map(function (item) {
						return $.extend(nodeVals[0], { 'relationship': '110' });
					}));
			}
		}
	}

	function positionDeleted($node, data)
	{
		state.orgChart.removeNodes($node);
		$('#selected-node').val('').data('node', null);
		eventSink.clearFilterResult();
	}

	eventSink.newPosition = newPosition;
	eventSink.positionDeleted = positionDeleted;
	eventSink.filterNodes = filterNodes;
	eventSink.clearFilterResult = clearFilterResult;
}

