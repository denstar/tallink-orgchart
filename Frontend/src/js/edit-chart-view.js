if (typeof module === 'object')
{
	$ = require('jquery');
	isOwnManager = require('utils-orgchart').isOwnManager;
	getAllPositions = require('utils-orgchart').getAllPositions;
	makePosition = require('utils-orgchart').makePosition;
	module.exports = {
		initEditChartView: initEditChartView
	};
}

function initEditChartView(eventSink, state, api, popup)
{
	eventSink.orgChartCreated = function(oc)
	{
		if (!oc) return;

		oc.$chartContainer.on('click', '.node', function() {
			var $this = $(this);
			$('#selected-node').val($this.find('.title').text()).data('node', $this);
		});

		oc.$chartContainer.on('click', state.chartNode, function(event) {
			if (!$(event.target).closest('.node').length) {
				$('#selected-node').val('');
			}
		});
	}

	eventSink.employeesData = loadEmployees;

	$('input[name="chart-state"]').on('click', function() {
		$(state.chartNode).toggleClass('edit-state', this.value !== 'view');
		$('#edit-panel').toggleClass('edit-state', this.value === 'view');
		if ($(this).val() === 'edit') {
			$(state.chartNode).find('tr').removeClass('hidden')
				.find('td').removeClass('hidden')
				.find('.node').removeClass('slide-up slide-down slide-right slide-left');
		} else {
			$('#btn-reset').trigger('click');
		}
	});

	$('input[name="node-type"]').on('click', function() {
		var $this = $(this);
		if ($this.val() === 'parent') {
			$('#edit-panel').addClass('edit-parent-node');
			$('#new-nodelist').children(':gt(0)').remove();
		} else {
			$('#edit-panel').removeClass('edit-parent-node');
		}
	});

	$('#btn-remove-input').on('click', function() {
		var inputs = $('#new-nodelist').children('li');
		if (inputs.length > 1) {
			inputs.last().remove();
		}
	});

	$('#btn-add-nodes').on('click', function()
	{
		var $chartContainer = $('#chart-container'),
			newPosition,
			val = $('#new-node-employee').val(),
			item = val != null ? state.employees.find(r => r.id == val) : null;
		if (item)
			newPosition = { 'name': item.title, 'title': $('#new-node-position').val(), signingAuthority: $('#new-node-signing-authority').is(":checked"), 'employee': item };

		addPosition(newPosition, $('#selected-node').data('node'));
	});

	$('#btn-delete-nodes').on('click', function() {
		var $node = $('#selected-node').data('node');
		if (!$node) {
			popup.alert('Please select one node in orgchart');
			return;
		} else if ($node[0] === $(state.chartNode).find('.node:first')[0]) {
			if (!popup.confirm('Are you sure you want to delete the whole chart?'))
				return;
		}
		api.deletePosition($node[0].id)
			.done(data => eventSink.positionDeleted($node, data))
			.fail((xhr, textStatus, errorThrown) => popup.alert(textStatus));
	});

	$('#btn-reset').on('click', function() {
		$(state.chartNode).find('.focused').removeClass('focused');
		$('#selected-node').val('');
		$('#new-nodelist').find('input:first').val('').parent().siblings().remove();
		$('#node-type-panel').find('input').prop('checked', false);
	});

	function addPosition(newPosition, $node) {

		var $chartContainer = $('#chart-container'),
			chartIsEmpty = !$chartContainer.children(state.chartNode).find('.node').length;

		if (!$node && !chartIsEmpty) {
			popup.alert('Please specify position manager');
			return;
		}

		if (!newPosition) {
			popup.alert('Please input value for new node');
			return;
		}

		if (!chartIsEmpty)
			newPosition.managerId = $node[0].id;
		else
		{
			if (!newPosition.title)
				newPosition.title = 'CEO';
		}

		if (!newPosition.title)
		{
			popup.alert('Please input position name');
			return;
		}

		var data = ($chartContainer.children(state.chartNode).data('options') || {}).data || {};

		if (isOwnManager(getAllPositions(data), newPosition))
		{
			popup.alert('Employee cannot be his own manager');
			return;
		}

		api.addPosition(newPosition)
			.done(data => eventSink.newPosition(data, $node))
			.fail((xhr, textStatus, errorThrown) => popup.alert(textStatus));
	}

	function loadEmployees(employees)
	{
		var selects = $('#new-node-employee');

		for (var i = 0; i < selects.length; i++)
			if (selects.eq(i).find('option').length == 0)
				for (var j = 0; j < employees.length; j++)
					selects.eq(i).append('<option value=' + employees[j].id + '>' + employees[j].title + '</option>');
	}

	function newEmployee(newEmployee)
	{
		var selects = $('#new-node-employee');

		for (var i = 0; i < selects.length; i++)
			selects.eq(i).append('<option value=' + newEmployee.id + '>' + newEmployee.title + '</option>');
	}

	eventSink.newEmployee = newEmployee;

	return {
		addPosition: addPosition,
		newEmployee: newEmployee
	}
}
