if (typeof module === 'object')
{
	module.exports = {
		makeChart: makeChart,
		isOwnManager: isOwnManager,
		getAllPositions: getAllPositions,
		makePosition: makePosition
	};
}

function makeChart(obj)
{
	var CEO = obj ? makePosition(obj) : null,
		chart = CEO;

	return chart;
}

function makePosition(position)
{
	position.name = position.employee.title;

	if (position.subordinates)
	{
		position.children = position.subordinates;
		delete position.subordinates;
	}

	if (position.children)
		for (var k in position.children)
			position.children[k] = makePosition(position.children[k]);

	return position;
}

function getAllPositions(data, res)
{
	res = res || {};
	res[data.id] = data;
	if (data.children)
		for (var i = 0; i < data.children.length; i++)
			getAllPositions(data.children[i], res);
	return res;
}

function isOwnManager(data, position)
{
	var managerId = position.managerId;
	while (managerId != null)
	{
		console.log('isOwnManager.managerId: ' + managerId);
		if (position.employee.id == data[managerId].employee.id)
		{
					console.log('isOwnManager.result: ' + true);
			return true;
		}
		managerId = data[managerId].managerId;
	}
					console.log('isOwnManager.result: ' + false);
	return false;
}
