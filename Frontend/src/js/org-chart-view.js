var loadChartView;

if (typeof module === 'object')
{
	$ = require('jquery');
	makeChart = require('utils-orgchart').makeChart;
	module.exports = {
		initOrgChartView: initOrgChartView,
		loadOrgChart: loadOrgChart
	};
}

function initOrgChartView(eventSink, state, api)
{
	loadChartView = function(data)
	{
		state.orgChart = null;

		if (data)
			state.orgChart = loadOrgChart(data);

		eventSink.orgChartCreated(state.orgChart);

		return state.orgChart;
	}

	eventSink.orgchartData = loadChartView;

	return {
		loadChartView: loadChartView,
		loadOrgChart: loadOrgChart
	}
}

function loadOrgChart(data, nodeContent)
{
	return $('#chart-container').orgchart({
		'data' : makeChart(data),
		'nodeContent': nodeContent || 'title',
		'nodeTemplate': nodeTemplate,
		'createNode': ($nodeDiv, data) => { $nodeDiv.data('item', data) }
	});
}

function showSigningAuthorityHtml(val)
{
	return val ? '<br><b>Signing authority</b>' : '';
}

function nodeTemplate(data)
{
	var strSigningAuthority = showSigningAuthorityHtml(data['signingAuthority']),
		strStyle = data['signingAuthority'] ? ' class="content" style="height:30pt;"' : ' class="content"';

	return '<div class="title">' + data['name'] + '</div>'
		+ '<div'+ strStyle +'>' + (data['title'] || '') + strSigningAuthority + '</div>'
}
