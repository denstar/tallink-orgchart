var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var mocha = require('gulp-mocha');
var uglify = require('gulp-uglify');
var rename = require("gulp-rename");
var del = require('del');
var eslint = require('gulp-eslint');
var merge = require('merge-stream');
var csslint = require('gulp-csslint');
var cleanCSS = require('gulp-clean-css');
var sourcemaps = require('gulp-sourcemaps');
var testcafe = require('gulp-testcafe');
var concat = require('gulp-concat');
var runSequence = require('run-sequence');
var paths = {
	src: 'src',
	srcFiles: 'src/**/*',
	srcHTML: 'src/**/*.html',
	srcCSS: 'src/css/*.css',
	srcJS: 'src/js/*.js',
	app: 'app',
	appFiles: 'app/**/*',
	appIndex: 'app/index.html',
	appHTML: 'app/**/*.html',
	appCSSFolder: 'app/css',
	appFontsFolder: 'app/fonts',
	appCSS: 'app/**/*.css',
	appJSFolder: 'app/js',
	appJS: 'app/**/*.js',
	dist: 'dist',
	distHTML: 'dist/*.html',
	distCSS: 'dist/css/*.css',
	distCSSFolder: 'dist/css',
	distJS: 'dist/js/*.js',
	distJSFolder: 'dist/js',
	orgchartJS: 'assets/orgchart/js/*.js',
	orgchartCSS: 'assets/orgchart/css/*.css'
};

gulp.task('unit-tests', function () {
	return gulp.src(['test/unit/*.js'], {read: false})
		.pipe(mocha({reporter: 'spec'}));
});

gulp.task('integration-tests', function () {
	return gulp.src(['test/integration/*.js'], {read: false})
		.pipe(mocha({reporter: 'spec'}));
});

gulp.task('addAssets', function () {

	var fontawesomeCSS = gulp.src('node_modules/font-awesome/css/font-awesome.min.css')
		.pipe(gulp.dest(paths.appCSSFolder));

	var fontawesomeFonts = gulp.src('node_modules/font-awesome/fonts/*')
		.pipe(gulp.dest(paths.appFontsFolder));

	var cssFiles = gulp.src([paths.distCSS, paths.orgchartCSS])
		.pipe(gulp.dest(paths.appCSSFolder));

	var jsFiles = gulp.src([
			paths.distJS,
			paths.orgchartJS,
			'node_modules/jquery/dist/jquery.min.js',
			'node_modules/jquery-mockjax/dist/jquery.mockjax.min.js',
			'node_modules/html2canvas/dist/html2canvas.min.js',
			'node_modules/jspdf/dist/jspdf.min.js'
		])
		.pipe(gulp.dest(paths.appJSFolder));

	var htmlFiles = gulp.src(paths.distHTML)
		.pipe(gulp.dest(paths.app));

	return merge(fontawesomeCSS, fontawesomeFonts, cssFiles, jsFiles, htmlFiles);

});

gulp.task('e2e-tests', ['addAssets'], function () {
	return gulp.src('test/e2e/**/test.js')
		.pipe(testcafe({ browsers: ['chrome:headless'] }));
});

gulp.task('cleanupJS', function() {
	return del([paths.distJSFolder + '/**']);
});

gulp.task('eslint', function () {
	return gulp.src(paths.srcJS) 
		.pipe(eslint.format())
		.pipe(eslint.failOnError());
});

gulp.task('js', ['cleanupJS', 'eslint'], function () {
	return gulp.src(paths.srcJS)
		.pipe(concat('bundle.js'))
		.pipe(gulp.dest(paths.distJSFolder))
		.pipe(sourcemaps.init())
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest(paths.distJSFolder))
});

gulp.task('cleanupCSS', function() {
	return del([paths.distCSSFolder + '/**']);
});

gulp.task('csslint', function() {
	gulp.src(paths.srcCSS)
		.pipe(csslint({
			'adjoining-classes': false,
			'box-sizing': false,
			'box-model': false,
			'fallback-colors': false,
			'order-alphabetical': false
		}))
		.pipe(csslint.formatter());
});

gulp.task('css', ['cleanupCSS'], function () {
	return gulp.src(paths.srcCSS)
		.pipe(concat('style.css'))
		.pipe(gulp.dest(paths.distCSSFolder))
		.pipe(cleanCSS())
		.pipe(gulp.dest(paths.distCSSFolder));
});

gulp.task('html', function () {
	return gulp.src(paths.srcHTML)
		.pipe(gulp.dest(paths.dist));
});

gulp.task('cleanupDist', function() {
	return del([paths.dist + '/**']);
});

gulp.task('build', function(cb)
{
	return runSequence('cleanupDist', 
		['js', 'css', 'html'],
		'addAssets',
		cb);
});

gulp.task('reload', function (done) {
	browserSync.reload();
	done();
});

gulp.task('serve', ['tests', 'build'], function () {
	browserSync.init({
		server: {
			baseDir: paths.app
		}
	});
	gulp.watch(paths.srcFiles, ['build']);
	gulp.watch(paths.appIndex, ['reload']);
});

gulp.task('tests', function(cb)
{
	return runSequence('unit-tests',
		'integration-tests',
		cb);
});
